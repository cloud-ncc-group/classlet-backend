import waitress
from flask import Flask
from flask_cors import CORS

from file_api_server import files
from util import request_handler

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/home/server/uploads'
CORS(app)


@app.route('/list-all', methods=['GET'])
def list_all_route(): return request_handler(files.list_all)


@app.route('/download', methods=['GET'])
def download_route(): return request_handler(files.download)


@app.route('/upload', methods=['POST'])
def upload_file_route(): return request_handler(files.upload_file)


@app.route('/delete', methods=['DELETE'])
def delete_file_route(): return request_handler(files.delete_file)


@app.route('/delete-all', methods=['DELETE'])
def delete_all_route(): return request_handler(files.delete_all)


if __name__ == "__main__":
	waitress.serve(app, host='0.0.0.0', port=1234)
