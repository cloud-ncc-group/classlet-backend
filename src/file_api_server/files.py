import json
import os
import shutil

from flask import request, send_from_directory, Response
from werkzeug.utils import secure_filename

from util import ok, error

upload_folder = '/home/server/uploads'


def list_all() -> Response:
	json_list = json.dumps(os.listdir(upload_folder))
	return ok(json_list)


def download():
	filename = request.args.get('file')
	if filename is None:
		return error('Argument `file` was not specified')
	filename = secure_filename(filename)

	if os.path.isfile(os.path.join(upload_folder, filename)):
		return send_from_directory(directory=upload_folder, filename=filename), 200
	else:
		return error(f'File {filename} does not exist')


def upload_file() -> Response:
	# check if the request has the file part
	if 'file' not in request.files:
		return error('No file attached')
	file = request.files['file']

	# if user does not select file, browser also
	# submit an empty part without filename
	if file.filename == '':
		return error('No file selected')

	filename = secure_filename(file.filename)

	if os.path.isfile(os.path.join(upload_folder, filename)):
		message = f'File {filename} was overwritten'
	else:
		message = f'File {filename} was successfully uploaded'

	file.save(os.path.join(upload_folder, filename))
	return ok(message)


def delete_file() -> Response:
	filename = request.args.get('file')
	if filename is None:
		return error('Argument `file` was not specified')
	filename = secure_filename(filename)

	if os.path.isfile(os.path.join(upload_folder, filename)):
		os.remove(os.path.join(upload_folder, filename))
		return ok(f'File {filename} was successfully deleted')
	else:
		return error(f'File {filename} does not exist')


def delete_all() -> Response:
	for root, dirs, files in os.walk(upload_folder):
		for f in files:
			os.unlink(os.path.join(root, f))
		for d in dirs:
			shutil.rmtree(os.path.join(root, d))

	return ok(f'All files were successfully deleted')
