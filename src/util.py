import traceback
from typing import Callable, Any

from flask import Response


def ok(message='', status=200):
	print(message)
	return Response(response=message, status=status)


def error(message, status=400):
	print('Error: ' + message)
	return Response(response='Error: ' + message, status=status)


def request_handler(function: Callable[[], Any]):
	try:
		return function()
	except Exception as e:
		print(traceback.format_exc())
		return error(str(e))
