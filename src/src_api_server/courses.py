from flask import request, Response, jsonify
from google.cloud import firestore

from util import error, ok

db = firestore.Client()
course_collection = db.collection('courses')


def create() -> Response:
	# Get json body from the request
	course_data = request.get_json()

	# Return error if there is no json body
	if not course_data:
		return error('No JSON body found. Please include it as a body of your request.')

	# Define and initialise the schema of the document
	data = {
		'code': course_data['code'],
		'name': course_data['name'],
		'credit': course_data['credit'],
		'instructor_name': course_data['instructor_name']
	}

	# Add a new course in collection 'courses' with randomly generated key
	course_collection.add(data)

	# Return successful info message
	return ok(f"Successfully added course: {course_data['name']}")


def update() -> Response:
	# Get json body from the request
	course_data = request.get_json()

	# Return error if there is no json body
	if not course_data:
		return error('No JSON body found. Please include it as a body of your request.')

	# Get the key of the course in the database
	key = course_data['key']
	data = {
		'code': course_data['code'],
		'name': course_data['name'],
		'credit': course_data['credit'],
		'instructor_name': course_data['instructor_name']
	}

	# Update the course based on its key
	course_collection.document(key).set(data)

	# Return successful info message
	return ok(f"Successfully updated course: {course_data['name']}")


def read() -> Response:
	# Get list of all course documents
	docs = course_collection.stream()

	# Map those document objects to python dictionaries
	courses = {}
	for doc in docs:
		courses[doc.id] = doc.to_dict()

	# Return the JSON representation of all courses
	return jsonify(courses)


def delete() -> Response:
	# Get course key from the request
	course_key = request.args.get('key')

	# Return error if there is no `key` parameter
	if not course_key:
		return error('Request parameter `key` was not found.')

	# Get the document to check its existence
	doc = course_collection.document(course_key)

	# Return error if the course does not exist
	if not doc.get().exists:
		return error(f'Course with the key `{course_key}` does not exist')

	# Check if there are no registrations with this course
	regs = db.collection('registrations').where('course_key', '==', course_key).stream()
	not_empty = list(i for i in regs)
	if not_empty:
		return error(f'Course with the key `{course_key}` is used in 1 or more registrations. '
		             f'Delete those registrations first.', 403)

	# Delete the document with the given key
	course_collection.document(course_key).delete()

	return ok(f'Successfully deleted course with the key: {course_key}')
