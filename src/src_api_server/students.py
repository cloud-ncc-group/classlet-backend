from flask import request, Response, jsonify
from google.cloud import firestore

from util import error, ok

db = firestore.Client()
student_collection = db.collection('students')


def create() -> Response:
	# Get json body from the request
	student_data = request.get_json()

	# Return error if there is no json body
	if not student_data:
		return error('No JSON body found. Please include it as a body of your request.')

	# Define and initialise the schema of the document
	data = {
		'id': student_data['id'],
		'name': student_data['name'],
		'department': student_data['department'],
		'cgpa': student_data['cgpa'],
		'year': student_data['year'],
	}

	# Add a new student in collection 'students' with randomly generated key
	student_collection.add(data)

	# Return successful info message
	return ok(f"Successfully added student: {student_data['name']}")


def update() -> Response:
	# Get json body from the request
	student_data = request.get_json()

	# Return error if there is no json body
	if not student_data:
		return error('No JSON body found. Please include it as a body of your request.')

	# Get the key of the student in the database
	key = student_data['key']
	data = {
		'id': student_data['id'],
		'name': student_data['name'],
		'department': student_data['department'],
		'cgpa': student_data['cgpa'],
		'year': student_data['year'],
	}

	# Update the student based on its key
	student_collection.document(key).set(data)

	# Return successful info message
	return ok(f"Successfully updated student: {student_data['name']}")


def read() -> Response:
	# Get list of all student documents
	docs = student_collection.stream()

	# Map those document objects to python dictionaries
	students = {}
	for doc in docs:
		students[doc.id] = doc.to_dict()

	# Return the JSON representation of all students
	return jsonify(students)


def delete() -> Response:
	# Get student key from the request
	student_key = request.args.get('key')

	# Return error if there is no `key` parameter
	if not student_key:
		return error('Request parameter `key` was not found.')

	# Get the document to check its existence
	doc = student_collection.document(student_key)

	# Return error if the course does not exist
	if not doc.get().exists:
		return error(f'Student with the key `{student_key}` does not exist')

	# Check if there are no registrations with this student
	regs = db.collection('registrations').where('student_key', '==', student_key).stream()
	not_empty = list(i for i in regs)
	if not_empty:
		return error(f'Student with the key `{student_key}` is used in 1 or more registrations. '
		             f'Delete those registrations first.', 403)

	# Delete the document with the given key
	student_collection.document(student_key).delete()

	return ok(f'Successfully deleted student with the key: {student_key}')
