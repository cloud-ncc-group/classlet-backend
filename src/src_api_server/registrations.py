from flask import request, Response, jsonify
from google.cloud import firestore

from util import error, ok

db = firestore.Client()
student_collection = db.collection('students')
registration_collection = db.collection('registrations')
course_collection = db.collection('courses')


def create() -> Response:
	# Get json body from the request
	registration_data = request.get_json()

	# Return error if there is no json body
	if not registration_data:
		return error('No JSON body found. Please include it as a body of your request.')

	# Define and initialise the schema of the document
	data = {
		'student_key': registration_data['student_key'],
		'course_key': registration_data['course_key'],
		'category': registration_data['category'],
	}

	# Add a new registration in collection 'students' with randomly generated key
	registration_collection.add(data)

	# Return successful info message
	return ok(f"Successfully added registration: "
	          f"{registration_data['student_key']} <=> {registration_data['course_key']}")


def update() -> Response:
	# Get json body from the request
	registration_data = request.get_json()

	# Return error if there is no json body
	if not registration_data:
		return error('No JSON body found. Please include it as a body of your request.')

	# Get the key of the course in the database
	key = registration_data['key']
	data = {
		'student_key': registration_data['student_key'],
		'course_key': registration_data['course_key'],
		'category': registration_data['category'],
	}

	# Update the course based on its key
	registration_collection.document(key).set(data)

	# Return successful info message
	return ok(f"Successfully updated registration with the key: {key}")


def read() -> Response:
	# Get list of source, registration & course documents
	reg_docs = registration_collection.stream()
	student_docs = student_collection.stream()
	course_docs = course_collection.stream()

	# Create a dictionary of students
	students = {}
	for doc in student_docs:
		students[doc.id] = doc.to_dict()

	# Create a dictionary of courses
	courses = {}
	for doc in course_docs:
		courses[doc.id] = doc.to_dict()

	# Create a dictionary of registrations and manually join courses with students
	registrations = {}
	for doc in reg_docs:
		registrations[doc.id] = doc.to_dict()
		student_key = registrations[doc.id]['student_key']
		course_key = registrations[doc.id]['course_key']
		registrations[doc.id]['student_id'] = students[student_key]['id']
		registrations[doc.id]['student_name'] = students[student_key]['name']
		registrations[doc.id]['course_code'] = courses[course_key]['code']
		registrations[doc.id]['course_name'] = courses[course_key]['name']

	# Return the JSON representation of all registrations
	return jsonify(registrations)


def delete() -> Response:
	# Get registration key from the request
	registration_key = request.args.get('key')

	# Return error if there is no `key` parameter
	if not registration_key:
		return error('Request parameter `key` was not found.')

	# Get the document to check its existence
	doc = registration_collection.document(registration_key)
	if doc.get().exists:
		# Delete the document with the given key
		registration_collection.document(registration_key).delete()
	else:
		return error(f'Registration with the key `{registration_key}` does not exist')

	return ok(f'Successfully deleted registration with the key: {registration_key}')
