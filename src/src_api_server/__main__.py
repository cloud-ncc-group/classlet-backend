import waitress
from flask import Flask
from flask_cors import CORS

from src_api_server import courses, students, registrations
from util import request_handler

app = Flask(__name__)
CORS(app)


# Add routes for students

@app.route('/students/create', methods=['POST'])
def student_create_route(): return request_handler(students.create)


@app.route('/students/read', methods=['GET'])
def student_read_route(): return request_handler(students.read)


@app.route('/students/update', methods=['PATCH'])
def student_update_route(): return request_handler(students.update)


@app.route('/students/delete', methods=['DELETE'])
def student_delete_route(): return request_handler(students.delete)


# Add routes for registrations

@app.route('/registrations/create', methods=['POST'])
def registration_create_route(): return request_handler(registrations.create)


@app.route('/registrations/read', methods=['GET'])
def registration_read_route(): return request_handler(registrations.read)


@app.route('/registrations/update', methods=['PATCH'])
def registration_update_route(): return request_handler(registrations.update)


@app.route('/registrations/delete', methods=['DELETE'])
def registration_delete_route(): return request_handler(registrations.delete)


# Add routes for courses

@app.route('/courses/create', methods=['POST'])
def course_create_route(): return request_handler(courses.create)


@app.route('/courses/read', methods=['GET'])
def course_read_route(): return request_handler(courses.read)


@app.route('/courses/update', methods=['PATCH'])
def course_update_route(): return request_handler(courses.update)


@app.route('/courses/delete', methods=['DELETE'])
def course_delete_route(): return request_handler(courses.delete)


if __name__ == "__main__":
	waitress.serve(app, host='0.0.0.0', port=2345)
