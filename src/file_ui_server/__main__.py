import waitress
from flask import Flask, render_template
from flask_cors import CORS

app = Flask(__name__,
            template_folder="/home/server/classlet-frontend/",
            static_folder='/home/server/classlet-frontend/static')
CORS(app)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
	return render_template('index.html')


if __name__ == "__main__":
	waitress.serve(app, host='0.0.0.0', port=80)
